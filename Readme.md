#Generador de sitio estático usando Python 3

Creo que esto es lo que estaba buscando, luego de andar lidiando con problemas de ataques y bloqueos en Wordpress y las actualizaciones de Owncloud que te dice que podés actualizar a la última versión pero que en realidad se te bloquea el sistema porque no podés pasar directamente de una versión mayor a otra mayor me hace pensar que en busca de la automatización de sistemas la gente pasa por alto cosas tan simples.

Mi respuesta a estos malestares, pasar mi humilde sitio a un formato HTML estático generado por un grupo de scripts de Python 3 usando como entrada archivos de texto con contenidos HTML y cargados por FTP, algo muy artesanal y manual pero parece que esto puede funcionar. También genera un feed RSS2 y un sitemap en formato XML y en Gzip, ambos son válidos.

Oportunidades para mejorar el código: básicamente la automatización para subir los nuevos archivos sean HTML, imágenes u otros.

#Changelog
## 2017-01-19
* Se agrega nueva sección para llevar un listado de libros.
* Refactorización de varios nombres de funciones.
* Modificación del menú.
* Eliminación de código viejo.
