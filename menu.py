# Generador de sitio estático usando Python 3
# Copyright (C) 2017  Edgar Pérez
#
# This file is part of Generador de sitio estático usando Python 3.
#
# Generador de sitio estático usando Python 3 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Generador de sitio estático usando Python 3 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Generador de sitio estático usando Python 3.  If not, see <http://www.gnu.org/licenses/>.

from nuevo_blog import crear_nuevo_blog
from nuevo_libro import crear_nuevo_libro
from actualizar_paginas import actualizar
from blog_listado import modificar_listado_blog
from libros_listado import modificar_listado_libros
from generar_feed import generar_feed
from generar_sitemap import generar_sitemap
import os


def menu():
    """Carga el menú
    Imprimie el menú en pantalla.
    """
    print("Menú de actualización del sitio")
    print("\t1 - Publicar nueva entrada en el blog")
    print("\t2 - Publicar nuevo libro")
    print("\t3 - Actualizar páginas")
    print("\t4 - Regenerar listado de entradas")
    print("\t5 - Regenerar listado de libros")
    print("\t6 - Regenerar feed")
    print("\t7 - Regenerar Sitemap")
    print("\t8 - Salir")


def continuar(opcion):
    """Limpiar pantalla
    Presionar Intro para continuar y limpiar la pantalla.
    """
    if opcion < 8:
        input("\nTarea terminada, presiona Intro para continuar.")


# Si se detecta una interrupción como Control + C se sale de forma silenciosa
try:
    # Ciclo principal
    while True:
        # Comprobamos la existencia de la variable opcion
        if 'opcion' not in locals():
            os.system('cls' if os.name == 'nt' else 'clear')
            opcion = 0

        menu()

        # Comprobamos si la entrada del usuario es un entero
        try:
            opcion = int(input("> "))
        except ValueError:
            pass
        else:
            if opcion == 1:
                crear_nuevo_blog()
            elif opcion == 2:
                crear_nuevo_libro()
            elif opcion == 3:
                actualizar()
            elif opcion == 4:
                modificar_listado_blog()
            elif opcion == 5:
                modificar_listado_libros()
            elif opcion == 6:
                generar_feed()
            elif opcion == 7:
                generar_sitemap()
            elif opcion >= 8:
                break
            continuar(opcion)
        finally:
            os.system('cls' if os.name == 'nt' else 'clear')
except KeyboardInterrupt:
    pass
