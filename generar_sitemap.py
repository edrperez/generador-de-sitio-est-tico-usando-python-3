#Generador de sitio estático usando Python 3
#Copyright (C) 2016  Edgar Pérez
#
#This file is part of Generador de sitio estático usando Python 3.
#
#Generador de sitio estático usando Python 3 is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Generador de sitio estático usando Python 3 is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with Generador de sitio estático usando Python 3.  If not, see <http://www.gnu.org/licenses/>.

import gzip
import shutil
import datetime
import os
from blog_listado import listado_archivos_blog

def comprimir_sitemap():
    """Comprimir sitemap.
    Comprime el sitema.xml usando GZ."""
    with open('public_html/sitemap.xml', 'rb') as f_in:
        with gzip.open('public_html/sitemap.xml.gz', 'wb') as f_out:
            shutil.copyfileobj(f_in, f_out)

def generar_sitemap():
    """Generar sitemap.
    Genera un sitemap simple de manera manual usando la fecha de modificación de las entradas del blog."""
    sitemap = """<?xml version="1.0" encoding="UTF-8"?>
    <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">"""

    listado = listado_archivos_blog(True)

    for i in listado:
        with open(i, 'r', encoding='utf8') as archivo:
            sitemap += '<url>'
            sitemap += '<loc>http://WWW.TUSITIO.COM/' + i[12:] + '</loc>'
            sitemap += '<lastmod>' + datetime.datetime.fromtimestamp(os.path.getmtime(i)).strftime('%Y-%m-%d') + '</lastmod>'
            sitemap += '<changefreq>monthly</changefreq>'
            sitemap += '<priority>0.5</priority>'
            sitemap += '</url>'

    sitemap += '</urlset>'

    with open('public_html/sitemap.xml', 'w', encoding='utf8') as final:
        final.write(sitemap)
    
    comprimir_sitemap()