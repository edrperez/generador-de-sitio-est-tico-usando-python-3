#Generador de sitio estático usando Python 3
#Copyright (C) 2016  Edgar Pérez
#
#This file is part of Generador de sitio estático usando Python 3.
#
#Generador de sitio estático usando Python 3 is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Generador de sitio estático usando Python 3 is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with Generador de sitio estático usando Python 3.  If not, see <http://www.gnu.org/licenses/>.

from bs4 import BeautifulSoup

def solo_titulo(archivo):
    """Devuelve el título de un archivo.
    Toma como base la primer etiqueta H1 del documento.
    """
    with open(archivo, 'r', encoding='utf8') as entrada:
        contenido = entrada.read()
        soup = BeautifulSoup(contenido, 'html.parser')
        titulo = soup.find('h1',).text
        titulo = titulo.strip()
    return titulo