#Generador de sitio estático usando Python 3
#Copyright (C) 2017  Edgar Pérez
#
#This file is part of Generador de sitio estático usando Python 3.
#
#Generador de sitio estático usando Python 3 is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Generador de sitio estático usando Python 3 is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with Generador de sitio estático usando Python 3.  If not, see <http://www.gnu.org/licenses/>.

import datetime
import os
from blog_listado import listado_archivos_blog
from bs4 import BeautifulSoup, Tag

def generar_feed():
    """Generar el feed.
    Genera el feed de manera manual tomando las últimas 5 entradas del blog por fecha de creación."""
    ahora = datetime.datetime.now().strftime('%a, %d %b %Y %H:%M:%S -0600')
    feed = """<?xml version="1.0" encoding="UTF-8"?>
    <rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
    <channel><title>Feed del Blog de TU NOMBRE</title>
    <link>http://WWW.TUSITIO.COM/</link>
    <description>Blog de temas de </description>
    <lastBuildDate>""" + ahora + """</lastBuildDate>
    <generator>Python propio</generator>
    <docs>http://blogs.law.harvard.edu/tech/rss</docs>"""

    listado = listado_archivos_blog(True)

    i = 0
    limite = 5

    while i <= limite and len(listado) > i:
        with open(listado[i], 'r', encoding='utf8') as archivo:
            feed += '<item>'
            contenido = archivo.read()
            soup = BeautifulSoup(contenido, 'html.parser')
            div = soup.find('div', id='cuerpo')
            titulo = div.find('h1').text.strip()
            tag = soup.find('h1')
            tag.replaceWith('')
            feed += '<title>' + titulo + '</title>'
            feed += '<link>http://WWW.TUSITIO.COM/' + listado[i][12:] + '</link>'
            descripcion = str(div)
            feed += '<description><![CDATA[' + descripcion + ']]></description>'
            feed += '<guid isPermaLink="true">http://WWW.TUSITIO.COM/' + listado[i][12:] + '</guid>'
            feed += '<pubDate>' + datetime.datetime.fromtimestamp(os.path.getctime(listado[i])).strftime('%a, %d %b %Y %H:%M:%S -0600') + '</pubDate>'
            feed += '</item>'
        i += 1
    feed += '</channel></rss>'

    with open('public_html/feed.xml', 'w', encoding='utf8') as final:
        final.write(feed)