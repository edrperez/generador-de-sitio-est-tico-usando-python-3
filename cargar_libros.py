# -*- coding: utf-8 -*-
from subprocess import call
import csv
from bs4 import BeautifulSoup
from nombre_archivo import nombre_archivo
import html

ruta = "C:\\Users\\egonzalez.INTRA\\Documents\\DOCS\\sitio\\public_html\\libros\\"

#19 es opinión, 1 es título, 2 y 3 son autores, 14 fecha de lectura
with open('listado.csv', newline='', encoding='utf-8') as archivo:
    contenido = csv.reader(archivo, delimiter=',', quotechar='"')
    for fila in contenido:
        if fila[19]:
            titulo = fila[1].strip()
            opinion = fila[19]
            cuerpo = '<h1>%s</h1>' % (titulo)
            autores = [fila[2], fila[4]]
            autores = filter(None, autores)
            fecha = fila[14].replace('/', '-')

            cuerpo += '<p><strong>Autor</strong>: %s</p>' % (', '.join(autores))
            cuerpo += '<p><strong>Fecha de lectura</strong>: %s</p>' % fecha
            cuerpo += '<h3>Opinión</h3>' + opinion

            with open('plantilla.html', 'r', encoding='utf8') as archivo:
                contenido = archivo.read().replace('{.titulo}', 'Opinión de ' + titulo)
                contenido = contenido.replace('{.descripcion}', html.escape(opinion[:160]))
                contenido = contenido.replace('{.autor}', 'Edgar Pérez')
                contenido = contenido.replace('{.keywords}', 'libro, opinión')
                contenido = contenido.replace('{.cuerpo}', cuerpo)

            with open('public_html/libros/' + nombre_archivo(titulo), 'w', encoding='utf8') as final:
                final.write(BeautifulSoup(contenido, 'html.parser').prettify())

            try:
                (a,m,d) = fecha.split('-')
            except ValueError:
                continue

            fecha = d + "-" + m + "-" + a + " 00:00:00"

            call(['nircmdc.exe', 'setfiletime', ruta + nombre_archivo(titulo), fecha, fecha, fecha])