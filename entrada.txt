<h1>Minimalismo</h1>
<p>Creo que esto es lo que estaba buscando, luego de andar lidiando con problemas de ataques y bloqueos en Wordpress y las actualizaciones de Owncloud que te dice que podés actualizar a la última versión pero que en realidad se te bloquea el sistema porque no podés pasar directamente de una versión mayor a otra mayor me hace pensar que en busca de la automatización de sistemas la gente pasa por alto cosas tan simples.</p>
<p>Mi respuesta a estos malestares, pasar mi humilde sitio a un formato HTML estático generado por un grupo de scripts de Python 3 usando como entrada archivos de texto con contenidos HTML y cargados por FTP, algo muy artesanal y manual pero parece que esto puede funcionar. También genera un feed RSS2 y un sitemap en formato XML y en Gzip, ambos son válidos.</p>
<p>Se puede revisar el <a href="http://www.edgarperezgonzalez.com/antiguo" target="_blank">sitio antiguo</a> y también he abierto un <a href="https://bitbucket.org/edrperez/generador-de-sitio-est-tico-usando-python-3" target="_blank">repositorio</a> en Bitbucket con el código de los scripts que estoy utilizando.</p>
<p>Oportunidades para mejorar el código: básicamente la automatización para subir los nuevos archivos sean HTML, imágenes u otros.</p>
<hr />
<p><strong>Creado:</strong> 2016-01-17</p>
<p><strong>Actualizado:</strong> 2016-01-17</p>