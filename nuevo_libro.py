#Generador de sitio estático usando Python 3
#Copyright (C) 2017  Edgar Pérez
#
#This file is part of Generador de sitio estático usando Python 3.
#
#Generador de sitio estático usando Python 3 is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Generador de sitio estático usando Python 3 is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with Generador de sitio estático usando Python 3.  If not, see <http://www.gnu.org/licenses/>.

from nombre_archivo import nombre_archivo
from contenido_tag import solo_titulo
from bs4 import BeautifulSoup
from libros_listado import modificar_listado_libros
from generar_feed import generar_feed
from generar_sitemap import generar_sitemap

def nuevo_libro(parametros):
    """Crear un nueva lectura.
    Toma los contenidos de libro.txt, extrae el título, recibe los valores para el HTML
    escribe todo al nuevo archivo y da indentación a las etiquetas.
    """
    libro = 'libro.txt'
    with open(libro, 'r', encoding='utf8') as archivo:
        cuerpo = archivo.read()
    titulo = solo_titulo(libro)

    with open('plantilla.html', 'r', encoding='utf8') as archivo:
        contenido = archivo.read().replace('{.titulo}', titulo)
        contenido = contenido.replace('{.descripcion}', parametros[0])
        contenido = contenido.replace('{.autor}', parametros[1])
        contenido = contenido.replace('{.keywords}', parametros[2])
        contenido = contenido.replace('{.cuerpo}', cuerpo)
    
    with open('public_html/libros/' + nombre_archivo(titulo), 'w', encoding='utf8') as final:
        final.write(BeautifulSoup(contenido, 'html.parser').prettify())

def crear_nuevo_libro():
    """Llama al proceso de creación de un nuevo libro.
    Recibe los datos y llama a la función para crear un nuevo libro.
    """
    descripcion = input('Descripción: ')
    autor = input('Autor: ')
    keywords = input('Keywords: ')
    parametros = [descripcion, autor, keywords]

    nuevo_libro(parametros)
    modificar_listado_libros()
    generar_feed()
    generar_sitemap()