#Generador de sitio estático usando Python 3
#Copyright (C) 2017  Edgar Pérez
#
#This file is part of Generador de sitio estático usando Python 3.
#
#Generador de sitio estático usando Python 3 is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Generador de sitio estático usando Python 3 is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with Generador de sitio estático usando Python 3.  If not, see <http://www.gnu.org/licenses/>.

from contenido_tag import solo_titulo
from bs4 import BeautifulSoup

def actualizar_pagina(valores):
    """Actualiza páginas que no son del blog.
    Actualiza ciertas páginas que no están dentro del blog, el listado es personalizable modificando
    el diccionario 'paginas', en ese diccionario la clave 'archivo' se refiere al archivo de texto de entrada, 
    ese nombre tomará también el archivo HTML a generar.
    """
    with open(valores['archivo'] + '.txt', 'r', encoding='utf8') as archivo:
        cuerpo = archivo.read()
    titulo = solo_titulo(valores['archivo'] + '.txt')
    
    with open('plantilla.html', 'r', encoding='utf8') as archivo:
        contenido = archivo.read().replace('{.titulo}', titulo)
        contenido = contenido.replace('{.descripcion}', valores['descripcion'])
        contenido = contenido.replace('{.autor}', 'TU NOMBRE')
        contenido = contenido.replace('{.keywords}', valores['keywords'])
        contenido = contenido.replace('{.cuerpo}', cuerpo)
    
    with open('public_html/' + valores['archivo'] + '.html', 'w', encoding='utf8') as final:
        final.write(BeautifulSoup(contenido, 'html.parser').prettify())

def actualizar():
    paginas = dict()

    paginas['Sobre mi'] = {'descripcion':'Sitio de Edgar Pérez', 'keywords':'edgar perez, guatemala', 'archivo':'index'}
    paginas['CV y Linkedin'] = {'descripcion':'Curriculum Vitae de Edgar Pérez, Guatemala', 'keywords':'linkedin, cv, curriculum, edgar perez, guatemala', 'archivo':'mi-cv-y-linkedin'}
    paginas['Contacto'] = {'descripcion':'Contactar a Edgar Pérez', 'keywords':'contacto, email, twitter', 'archivo':'contacto'}

    for i in paginas:
        decision = input('Actualizar página: ' + i + '. (S/N)')
        if decision.lower() == 's':
            actualizar_pagina(paginas[i])