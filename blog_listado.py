#Generador de sitio estático usando Python 3
#Copyright (C) 2017  Edgar Pérez
#
#This file is part of Generador de sitio estático usando Python 3.
#
#Generador de sitio estático usando Python 3 is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Generador de sitio estático usando Python 3 is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with Generador de sitio estático usando Python 3.  If not, see <http://www.gnu.org/licenses/>.

import os
from contenido_tag import solo_titulo
from bs4 import BeautifulSoup

def listado_archivos_blog(orden):
    """Lista archivos.
    Devuelve el listado de archivos del blog ordenados por fecha de creación.
    """
    ordenado = []

    for file in os.listdir('public_html/blog/'):
        if file.endswith('.html'):
            ordenado.append('public_html/blog/' + file)

    final = sorted(ordenado, key=os.path.getctime, reverse=orden)
    return final

def modificar_listado_blog():
    """Modifica el listado de entradas del blog.
    Recuperar el listado de archivos, asigna títulos, construye el HTML y lo agrega a un nuevo documento.
    """
    cuerpo = '<ol>'
    listado = listado_archivos_blog(True)

    for i in listado:
        cuerpo += '<li><a href="' + i[12:] + '">' + solo_titulo(i) + '</a></li>'

    cuerpo += '</ol>'
    
    with open('plantilla.html', 'r', encoding='utf8') as archivo:
        contenido = archivo.read().replace('{.titulo}', 'Blog')
        contenido = contenido.replace('{.descripcion}', 'Listado de entradas del Blog')
        contenido = contenido.replace('{.autor}', 'TU NOMBRE')
        contenido = contenido.replace('{.keywords}', 'blog')
        contenido = contenido.replace('{.cuerpo}', cuerpo)
    
    with open('public_html/blog.html', 'w', encoding='utf8') as final:
        final.write(BeautifulSoup(contenido, 'html.parser').prettify())