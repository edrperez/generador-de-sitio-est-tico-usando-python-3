#Generador de sitio estático usando Python 3
#Copyright (C) 2016  Edgar Pérez
#
#This file is part of Generador de sitio estático usando Python 3.
#
#Generador de sitio estático usando Python 3 is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Generador de sitio estático usando Python 3 is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with Generador de sitio estático usando Python 3.  If not, see <http://www.gnu.org/licenses/>.

from nombre_archivo import nombre_archivo
from contenido_tag import solo_titulo
from bs4 import BeautifulSoup
from blog_listado import modificar_listado_blog
from generar_feed import generar_feed
from generar_sitemap import generar_sitemap

def nuevo_archivo(parametros):
    """Crear un nuevo archivo para el blog.
    Toma los contenidos de entrada.txt, extrae el título, recibe los valores para el HTML
    escribe todo al nuevo archivo y da indentación a las etiquetas.
    """
    entrada = 'entrada.txt'
    with open(entrada, 'r', encoding='utf8') as archivo:
        cuerpo = archivo.read()
    titulo = solo_titulo(entrada)

    with open('plantilla.html', 'r', encoding='utf8') as archivo:
        contenido = archivo.read().replace('{.titulo}', titulo)
        contenido = contenido.replace('{.descripcion}', parametros[0])
        contenido = contenido.replace('{.autor}', parametros[1])
        contenido = contenido.replace('{.keywords}', parametros[2])
        contenido = contenido.replace('{.cuerpo}', cuerpo)
    
    with open('public_html/blog/' + nombre_archivo(titulo), 'w', encoding='utf8') as final:
        final.write(BeautifulSoup(contenido, 'html.parser').prettify())

def crear_nuevo_blog():
    """Llama al proceso de creación de una nueva entrada.
    Recibe los datos y llama a la función para crear una nueva entrada en el blog.
    """
    descripcion = input('Descripción: ')
    autor = input('Autor: ')
    keywords = input('Keywords: ')
    parametros = [descripcion, autor, keywords]

    nuevo_archivo(parametros)
    modificar_listado_blog()
    generar_feed()
    generar_sitemap()