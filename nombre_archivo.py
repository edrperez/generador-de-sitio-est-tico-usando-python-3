#Generador de sitio estático usando Python 3
#Copyright (C) 2016  Edgar Pérez
#
#This file is part of Generador de sitio estático usando Python 3.
#
#Generador de sitio estático usando Python 3 is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Generador de sitio estático usando Python 3 is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with Generador de sitio estático usando Python 3.  If not, see <http://www.gnu.org/licenses/>.

import unicodedata
import string

def nombre_archivo(cadena):
    """Devolver nombre del archivo.
    Normaliza el nombre del documento reemplazando tildes y otros caracteres
    no ASCII, también reemplaza espacios en blanco por _ y agrega .html al final
    del archivo."""
    cadena = unicodedata.normalize('NFKD', cadena)
    valid_chars = "-_ %s%s" % (string.ascii_letters, string.digits)
    nombre = ''.join(c for c in cadena if c in valid_chars)
    nombre = nombre.replace(' ','_').strip().lower() + '.html'
    return nombre